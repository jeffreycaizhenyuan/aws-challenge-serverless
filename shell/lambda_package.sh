#!/usr/bin/env bash

BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. $BASEDIR/functions.sh

header "Package lambda function in zip"

docker-compose run --rm --workdir /opt/app/app worker zip -r lambda_function_payload.zip .
rm -rf terraform/lambda_function_payload.zip
mv app/lambda_function_payload.zip terraform