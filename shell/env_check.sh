#!/usr/bin/env bash

BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. $BASEDIR/functions.sh

header "Checking workstation setup correctly before running"

log ".env file exists"
if [ ! -f $BASEDIR/../.env ]; then
  cp $BASEDIR/../.env.template $BASEDIR/../.env
  log "[Error] Please update .env file at root of project"
  exit 1
fi

log "AWS credentials set"
if [ -z "$AWS_ACCESS_KEY_ID" ] || [ -z "$AWS_SECRET_ACCESS_KEY" ] || [ -z "$AWS_DEFAULT_REGION" ]; then
  log "[Error] AWS credentials not set, please set in .env file at root of project"
  exit 1
fi

