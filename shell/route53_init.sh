#!/usr/bin/env bash

BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. $BASEDIR/functions.sh

header "Create Route53 zone for domain ${DOMAIN_NAME}, if not yet"

# Function to check if the hosted zone exists
check_hosted_zone_exists() {
  docker-compose run --rm worker aws route53 list-hosted-zones | jq -r '.HostedZones[].Name' | grep -Fx "$DOMAIN_NAME."
}

# Function to create hosted zone
create_hosted_zone() {
  docker-compose run --rm worker aws route53 create-hosted-zone --name "$DOMAIN_NAME" --caller-reference "$DOMAIN_NAME-$(date +%s)"
}

# Main script logic
if check_hosted_zone_exists; then
  log "Hosted zone for '$DOMAIN_NAME' already exists."
else
  log "Hosted zone for '$DOMAIN_NAME' does not exist. Creating..."
  create_hosted_zone
  log "Hosted zone created."
fi
