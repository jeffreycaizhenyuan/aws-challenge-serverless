#!/usr/bin/env bash
BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. $BASEDIR/functions.sh

## main
header "Pre-requisites for application deployment"

# Create the S3 bucket for app assets if it doesn't exist
log "creating s3 bucket ${S3_BUCKET_APP} if not yet."
if ! check_s3_bucket_exists $S3_BUCKET_APP; then
  docker-compose run --rm worker aws s3 mb s3://$S3_BUCKET_APP --region $AWS_DEFAULT_REGION

  # Enable versioning on the S3 bucket
  docker-compose run --rm worker aws s3api put-bucket-versioning \
      --bucket $S3_BUCKET_APP \
      --versioning-configuration Status=Enabled
fi

# copy application assets to s3 bucket
log "copying application assets to s3 bucket..."
docker-compose run --rm worker aws s3 cp /opt/app/app/sbs-world-cup.jpeg s3://${S3_BUCKET_APP}/sbs-world-cup.jpeg
