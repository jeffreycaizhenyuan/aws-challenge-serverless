#!/user/bin/env bash
BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. $BASEDIR/functions.sh

## main
header "Pre-requisites for terraform setup"

# create the DynamoDB table for Terraform state locking if it doesn't exist
log "creating dynamodb table ${DYNAMODB_TABLE_NAME} if not yet."
if ! check_dynamodb_table_exists $DYNAMODB_TABLE_NAME; then
    docker-compose run --rm worker aws dynamodb create-table \
        --table-name $DYNAMODB_TABLE_NAME \
        --attribute-definitions AttributeName=LockID,AttributeType=S \
        --key-schema AttributeName=LockID,KeyType=HASH \
        --billing-mode PAY_PER_REQUEST \
        --region $AWS_DEFAULT_REGION
fi

# Create the S3 bucket for Terraform state if it doesn't exist
log "creating s3 bucket ${S3_BUCKET_TFSTATE} if not yet."
if ! check_s3_bucket_exists $S3_BUCKET_TFSTATE; then
    docker-compose run --rm worker aws s3 mb s3://$S3_BUCKET_TFSTATE --region $AWS_DEFAULT_REGION

    # Enable versioning on the S3 bucket
    docker-compose run --rm worker aws s3api put-bucket-versioning \
        --bucket $S3_BUCKET_TFSTATE \
        --versioning-configuration Status=Enabled
fi
