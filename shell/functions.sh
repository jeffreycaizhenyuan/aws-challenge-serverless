function log() {
  echo "  - $1"
}

function header() {
  echo "* $1"
}

check_dynamodb_table_exists() {
    docker-compose run \
      --rm worker \
      aws dynamodb describe-table --table-name $1 --region $AWS_DEFAULT_REGION > /dev/null 2>&1
}

check_s3_bucket_exists() {
    docker-compose run \
      --rm worker \
      aws s3 ls "s3://$1" > /dev/null 2>&1
}
