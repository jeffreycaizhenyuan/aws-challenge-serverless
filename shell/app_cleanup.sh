#!/usr/bin/env bash
BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. $BASEDIR/functions.sh

## main
header "Delete S3 bucket for app assets, if not yet"

# Create the S3 bucket for app assets if it doesn't exist
log "deleting s3 bucket ${S3_BUCKET_APP} if not yet."
if check_s3_bucket_exists $S3_BUCKET_APP; then
  docker-compose run --rm worker /usr/bin/python3 /opt/app/python/empty_and_delete_s3_bucket.py -b $S3_BUCKET_APP
fi
