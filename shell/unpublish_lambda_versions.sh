#!/usr/bin/env bash
BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. $BASEDIR/functions.sh

FUNCTION_NAME=$1
LATEST=$2

## main
header "Unpublishing all versions and aliases for Lambda function ${FUNCTION_NAME}"

# Get all version numbers except $LATEST
versions=$(aws lambda list-versions-by-function --function-name $FUNCTION_NAME | jq -r '.Versions[] | select(.Version != "$LATEST") | .Version')

# Loop over the versions
for version in $versions; do
    # Get all aliases for the current version
    aliases=$(aws lambda list-aliases --function-name $FUNCTION_NAME --function-version $version | jq -r '.Aliases[].Name')
    
    # Loop over the aliases and delete them
    for alias in $aliases; do
        log "Deleting alias $alias for version $version of $FUNCTION_NAME"
        aws lambda delete-alias --function-name $FUNCTION_NAME --name $alias
    done

    # Now delete the version
    log "Deleting version $version of $FUNCTION_NAME"
    aws lambda delete-function --function-name $FUNCTION_NAME --qualifier $version
done

log "All versions and aliases deleted (except $LATEST)"
