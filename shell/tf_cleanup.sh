#!/usr/bin/env bash
BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. $BASEDIR/functions.sh

## main
header "Cleanup for terraform setup"

# create the DynamoDB table for Terraform state locking if it doesn't exist
log "deleting dynamodb table ${DYNAMODB_TABLE_NAME}."
if check_dynamodb_table_exists $DYNAMODB_TABLE_NAME; then
  docker-compose run --rm worker aws dynamodb delete-table --table-name $DYNAMODB_TABLE_NAME --region $AWS_DEFAULT_REGION
fi

# Create the S3 bucket for Terraform state if it doesn't exist
log "deleting s3 bucket ${S3_BUCKET_TFSTATE}."
if check_s3_bucket_exists $S3_BUCKET_TFSTATE; then
  docker-compose run --rm worker /usr/bin/python3 /opt/app/python/empty_and_delete_s3_bucket.py -b $S3_BUCKET_TFSTATE
fi
