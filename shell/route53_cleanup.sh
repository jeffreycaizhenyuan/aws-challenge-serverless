#!/usr/bin/env bash

BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. $BASEDIR/functions.sh

header "Delete Route53 zone for domain ${DOMAIN_NAME}, if not yet"

# Function to get the hosted zone ID
get_hosted_zone_id() {
  docker-compose run --rm worker aws route53 list-hosted-zones | jq -r --arg domain "$DOMAIN_NAME." '.HostedZones[] | select(.Name == $domain) | .Id'
}

# Function to delete hosted zone
delete_hosted_zone() {
  local zone_id=$1
  docker-compose run --rm worker  aws route53 delete-hosted-zone --id "$zone_id"
}

# Main script logic
zone_id=$(get_hosted_zone_id)
if [ -n "$zone_id" ]; then
  log "Hosted zone for '$DOMAIN_NAME' exists with ID $zone_id. Deleting..."
  delete_hosted_zone "$zone_id"
  log "Hosted zone deleted."
else
  log "No hosted zone found for '$DOMAIN_NAME'."
fi
