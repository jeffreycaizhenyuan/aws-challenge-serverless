import base64
import os
import datetime
import pytz

def handler(event, context):
    # Path to the image in the local file system
    local_image_path = './sbs-world-cup.jpeg'

    # Read the image content
    with open(local_image_path, 'rb') as image_file:
        image_content = image_file.read()

    # Encode the image content in base64
    encoded_image = base64.b64encode(image_content).decode('utf-8')

    # Get current date and time in Australia/Sydney timezone
    sydney_tz = pytz.timezone('Australia/Sydney')
    current_time = datetime.datetime.now(sydney_tz).strftime("%Y/%m/%d %H:%M:%S %Z")

    # HTML content with the base64 encoded image
    html_content = f"""
    <!DOCTYPE html>
    <html>
    <head>
        <title>AWS Serverless Challenge</title>
    </head>
    <body>
        <img src="data:image/jpeg;base64,{encoded_image}" alt="World Cup Image">
        <br />
        <p>Date on Webserver is {current_time}</p>
    </body>
    </html>
    """

    return {
        'statusCode': 200,
        'statusDescription': "200 OK",
        'isBase64Encoded': False,
        'headers': {
            'Content-Type': 'text/html'
        },
        'body': html_content
    }
