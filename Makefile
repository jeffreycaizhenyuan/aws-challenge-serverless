## load and export .env
ifneq (,$(wildcard ./.env))
	include .env
	export
endif

## shorthand of docker-compose run prefix command
RUN=DOCKER_IMAGE_NAME=$(DOCKER_IMAGE_NAME) docker-compose run --rm --workdir /opt/app/terraform worker

## ops jobs
# build work image
worker_build:
	docker build -t ${DOCKER_IMAGE_NAME} -f docker/Dockerfile .
.PHONY: worker_build

# debug worker container
worker_debug:
	docker run --rm -it -v $(shell pwd):/opt/app ${DOCKER_IMAGE_NAME} /bin/bash
.PHONY: worker_debug

# push work image
worker_push:
	docker push ${DOCKER_IMAGE_NAME}
.PHONY: worker_push

## project init
init:
  # create route53 zone
	@bash shell/route53_init.sh
  # package lambda function code in zip
	@bash shell/lambda_package.sh
  # create app s3 bucket
	@bash shell/app_prepare.sh
.PHONY: init

## clean up
cleanup:
	@bash shell/route53_cleanup.sh
	@bash shell/app_cleanup.sh
.PHONY: cleanup

# check workstation is setup
env_check:
	@cd shell && bash env_check.sh
.PHONY: env_check


# terraform backend prepare, create dynamodb table and s3 bucket
tf_prepare:
	cd shell && bash tf_prepare.sh
.PHONY: tf_prepare

# terraform backend cleanup, delete dynamodb table and s3 bucket
tf_cleanup:
	cd shell && bash tf_cleanup.sh
.PHONY: tf_cleanup

# terraform init
tf_init: tf_prepare
	@echo "* Initialize terraform"
	$(RUN) terraform init \
		-backend-config="bucket=$(S3_BUCKET_TFSTATE)" \
		-backend-config="dynamodb_table=$(DYNAMODB_TABLE_NAME)" \
		-backend-config="region=$(AWS_DEFAULT_REGION)" \
		-backend-config="key=terraform/terraform.state" \
		-backend-config="encrypt=true"

# terraform plan
tf_plan: tf_init
	@echo "* Terraform plan"
	$(RUN) terraform plan \
		-var "serverless_bucket=$(S3_BUCKET_APP)" \
		-var "domain_name=$(DOMAIN_NAME)"
.PHONY: tf_plan

# terraform apply
tf_apply:
	@echo "* Terraform apply"
	$(RUN) terraform apply \
		-var "serverless_bucket=$(S3_BUCKET_APP)" \
		-var "domain_name=$(DOMAIN_NAME)" \
		-auto-approve
.PHONY: tf_apply

# terraform destroy
tf_destroy: lambda_unpublish
	@echo "* Terraform destroy"
	$(RUN) terraform destroy \
		-var "serverless_bucket=$(S3_BUCKET_APP)" \
		-var "domain_name=$(DOMAIN_NAME)" \
		-auto-approve
.PHONY: tf_apply

# unpublish all lambda versions, this is required before deleting lambda, or the ENI is still active and you'll be stuck with terraform destroy
lambda_unpublish:
	cd shell && bash unpublish_lambda_versions.sh dynamic_site
.PHONY: lambda_unpublish

# generate terraform docs
tf_docs:
	@echo "* Generate Terraform docs"
	$(RUN) terraform-docs markdown table . --output-file README.md
.PHONY: tf_docs