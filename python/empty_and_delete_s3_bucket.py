#!/usr/bin/env python3
'''
This script empty and delete a version enabled s3 bucket
'''
import boto3
import argparse

parser = argparse.ArgumentParser(description="Empty a specified S3 bucket with versioning and delete")
parser.add_argument('-b', '--bucket', required=True, help="S3 bucket name")
args = parser.parse_args()

BUCKET = args.bucket

s3 = boto3.resource('s3')
bucket = s3.Bucket(BUCKET)
bucket.object_versions.delete()
bucket.delete()