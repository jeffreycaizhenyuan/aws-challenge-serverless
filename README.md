# AWS Challenge of Serverless Web Application

A project that deploys a serverless web application in AWS.

See demo result at: [http://challenge.cdnlookup.site](http://challenge.cdnlookup.site)

![homepage](docs/homepage.png)

## Pre-requisit

Before deployment, please check the following pre-requisit have been met

- Linux environment with `bash` and `make`.
- `docker` and `docker-compose` installed.
- AWS credentials of your own account with root / amdin permission ready.
- Internet connectivity
- (Optional) you have your own domain and can update its DNS zone.

## Architecture

![architecture diagram](docs/architecture.drawio.png)

## How to deploy

1. **Update `.env` file**

Run pre-check script first.

```bash
make env_check
```

Find `.env` file in the project root and update the following env vars:

```bash
DEPLOYER_NAME=xxxx # lower case short name, used for s3 bucket postfix

AWS_ACCESS_KEY_ID=xxxxxxxxx
AWS_SECRET_ACCESS_KEY=xxxxxxxxx

DOMAIN_NAME=xxxxxxxxx.com # optional, this will create a route53 public zone.
```

The other env vars can be left as is.

2. **Initialize project**

```
make init
make tf_init
```

This will create pre-requesit assets for lambda / terraform / dns

See [Makefile](Makefile#L27) for details.

3. **(Optional) Change default vpc settings**

All terraform config can be found at [terraform/locals.tf](terraform/locals.tf). If you have an existing vpc with conflicting cidr, you might want to change it, otherwise, you can leave it as is.

4. **(Optional) Update your own domain's NS record to point to Route53**

In Route53, you'll find a hosted zone created as per your definition with `DOMAIN_NAME`. 

Record the NS servers and go to your domain register's website to create ns records with the values.

![public zone](docs/public_zone.png)

5. **Infra deployment**

```
make tf_plan
make tf_apply
```

6. **Test**

Once deployed, check the deployment by accessing:

- (optional) your own domain name as `DOMAIN_NAME` env var set. 
- Find the alb with name `serverless-alb` at AWS console "EC2" > "Load Balancer". Copy and access the "DNS name".

![ALB dns](docs/alb-dns.png)

- Due to a AWS/Terraform bug, you might need to go to the lambda function, make a slight dummy change of code and deploy / publish a new version. (to be investigated)

![ALB dns](docs/lambda.png)

7. **Clean up**

You can run the following command to decommission all deployment and preject related resources:

```
make tf_destroy
make tf_cleanup
make cleanup
```

# Appendex

## Folder structure

```
├── Makefile # main entry point
├── README.md
├── app # serverless website app code and assets
   └── ...
├── docker # devops worker container build scripts
│   └── ...
├── docker-compose.yml
├── docs
│   └── ..
├── python # python util scripts
│   └── ...
├── shell # deployment scripts
│   └── ...
└── terraform # terraform code
    └── ...
```

## Main components references

| Component | Reference |
|----------|----------|
| Lambda code | [lambda_function.py](app/lambda_function.py) |
| Terraform backend | [backend.tf](terraform/backend.tf), [Makefile](Makefile#L62) |
| VPC | [vpc.tf](terraform/vpc.tf), [terraform/modules/vpc](terraform/modules/vpc) |
| ALB | [alb.tf](terraform/alb.tf) |
| Lambda | [lambda.tf](terraform/lambda.tf) |
| VPC Private link | [privatelink.tf](terraform/private_links.tf) |
| Docker ops worker | [Dockerfile](docker/Dockerfile) |
| Deployment scripts | [shell](shell) |

## Terraform docs

See terraform docs at [terraform/README.md](terraform/README.md)
