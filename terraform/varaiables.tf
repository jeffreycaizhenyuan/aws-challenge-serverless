variable serverless_bucket {
  type        = string
  description = "s3 bucket to host serverless website assets"
}

variable domain_name {
  type        = string
  description = "dns name for the serverless site"
}