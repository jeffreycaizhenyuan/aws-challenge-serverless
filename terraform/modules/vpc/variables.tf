variable "vpc_cidr" {
    type        = string
}
variable "vpc_name" {
    type        = string
}
variable "subnet_public_a_cidr" {
    type        = string
}
variable "subnet_public_b_cidr" {
    type        = string
}
variable "subnet_public_c_cidr" {
    type        = string
}
variable "subnet_private_a_cidr" {
    type        = string
}
variable "subnet_private_b_cidr" {
    type        = string
}
variable "subnet_private_c_cidr" {
    type        = string
}
variable "subnet_data_a_cidr" {
    type        = string
}
variable "subnet_data_b_cidr" {
    type        = string
}
variable "subnet_data_c_cidr" {
    type        = string
}
variable "igw" {
    type        = number
}
variable "ngw" {
    type        = number
}
variable "three_az" {
    type        = number
}
variable "include_data_subnets" {
    type        = number
}