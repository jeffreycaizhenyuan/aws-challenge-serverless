# resources
resource "aws_internet_gateway" "igw" {
    count = var.igw
    vpc_id = aws_vpc.main.id
    tags = {
        Name = "${var.vpc_name}-igw"
    }
}

resource "aws_eip" "ngw_eip_a" {
    count    = var.ngw
    vpc      = true
}
resource "aws_nat_gateway" "ngw_private_a" {
    count    = var.ngw
    allocation_id = aws_eip.ngw_eip_a[0].id
    subnet_id     = aws_subnet.subnet_public_a.id
    tags = {
        Name = "ngw_private_a"
    }
}
resource "aws_eip" "ngw_eip_b" {
    count    = var.ngw
    vpc      = true
}
resource "aws_nat_gateway" "ngw_private_b" {
    count    = var.ngw
    allocation_id = aws_eip.ngw_eip_b[0].id
    subnet_id     = aws_subnet.subnet_public_b.id
    tags = {
        Name = "ngw_private_b"
    }
}
resource "aws_eip" "ngw_eip_c" {
    count    = var.ngw * var.three_az
    vpc      = true
}
resource "aws_nat_gateway" "ngw_private_c" {
    count    = var.ngw * var.three_az
    allocation_id = aws_eip.ngw_eip_c[0].id
    subnet_id     = aws_subnet.subnet_public_c[0].id
    tags = {
        Name = "ngw_private_c"
    }
}
