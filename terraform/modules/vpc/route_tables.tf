# resources
# public
resource "aws_route_table" "public_route_table_a" {
    vpc_id = aws_vpc.main.id
    tags = {
        Name = "public_route_table_a"
    }
}
resource "aws_route_table" "public_route_table_b" {
    vpc_id = aws_vpc.main.id
    tags = {
        Name = "public_route_table_b"
    }
}
resource "aws_route_table" "public_route_table_c" {
    count = var.three_az
    vpc_id = aws_vpc.main.id
    tags = {
        Name = "public_route_table_c"
    }
}
resource "aws_route_table_association" "public_a" {
    subnet_id      = aws_subnet.subnet_public_a.id
    route_table_id = aws_route_table.public_route_table_a.id
}
resource "aws_route_table_association" "public_b" {
    subnet_id      = aws_subnet.subnet_public_b.id
    route_table_id = aws_route_table.public_route_table_b.id
}
resource "aws_route_table_association" "public_c" {
    count = var.three_az
    subnet_id      = aws_subnet.subnet_public_c[0].id
    route_table_id = aws_route_table.public_route_table_c[0].id
}
# private
resource "aws_route_table" "private_route_table_a" {
    vpc_id = aws_vpc.main.id
    tags = {
        Name = "private_route_table_a"
    }
}
resource "aws_route_table" "private_route_table_b" {
    vpc_id = aws_vpc.main.id
    tags = {
        Name = "private_route_table_b"
    }
}
resource "aws_route_table" "private_route_table_c" {
    count = var.three_az
    vpc_id = aws_vpc.main.id
    tags = {
        Name = "private_route_table_c"
    }
}
resource "aws_route_table_association" "private_a" {
    subnet_id      = aws_subnet.subnet_private_a.id
    route_table_id = aws_route_table.private_route_table_a.id
}
resource "aws_route_table_association" "private_b" {
    subnet_id      = aws_subnet.subnet_private_b.id
    route_table_id = aws_route_table.private_route_table_b.id
}
resource "aws_route_table_association" "private_c" {
    count = var.three_az
    subnet_id      = aws_subnet.subnet_private_c[0].id
    route_table_id = aws_route_table.private_route_table_c[0].id
}
# data
resource "aws_route_table" "data_route_table_a" {
    count = var.include_data_subnets
    vpc_id = aws_vpc.main.id
    tags = {
        Name = "data_route_table_a"
    }
}
resource "aws_route_table" "data_route_table_b" {
    count = var.include_data_subnets
    vpc_id = aws_vpc.main.id
    tags = {
        Name = "data_route_table_b"
    }
}
resource "aws_route_table" "data_route_table_c" {
    count = var.include_data_subnets * var.three_az
    vpc_id = aws_vpc.main.id
    tags = {
        Name = "data_route_table_c"
    }
}
resource "aws_route_table_association" "data_a" {
    count = var.include_data_subnets
    subnet_id      = aws_subnet.subnet_data_a[0].id
    route_table_id = aws_route_table.data_route_table_a[0].id
}
resource "aws_route_table_association" "data_b" {
    count = var.include_data_subnets
    subnet_id      = aws_subnet.subnet_data_b[0].id
    route_table_id = aws_route_table.data_route_table_b[0].id
}
resource "aws_route_table_association" "data_c" {
    count = var.include_data_subnets * var.three_az
    subnet_id      = aws_subnet.subnet_data_c[0].id
    route_table_id = aws_route_table.data_route_table_c[0].id
}
