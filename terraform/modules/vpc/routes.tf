# resources
# public_routes
resource "aws_route" "r10" {
    count                     = var.igw
    route_table_id            = aws_route_table.public_route_table_a.id
    destination_cidr_block    = "0.0.0.0/0"
    gateway_id                = aws_internet_gateway.igw[0].id
    depends_on                = [aws_route_table.public_route_table_a, aws_internet_gateway.igw]
}
resource "aws_route" "r20" {
    count                     = var.igw
    route_table_id            = aws_route_table.public_route_table_b.id
    destination_cidr_block    = "0.0.0.0/0"
    gateway_id                = aws_internet_gateway.igw[0].id
    depends_on                = [aws_route_table.public_route_table_b, aws_internet_gateway.igw]
}
resource "aws_route" "r30" {
    count                     = var.igw * var.three_az
    route_table_id            = aws_route_table.public_route_table_c[0].id
    destination_cidr_block    = "0.0.0.0/0"
    gateway_id                = aws_internet_gateway.igw[0].id
    depends_on                = [aws_route_table.public_route_table_c, aws_internet_gateway.igw]
}

# private_routes
resource "aws_route" "r40" {
    count                     = var.ngw
    route_table_id            = aws_route_table.private_route_table_a.id
    destination_cidr_block    = "0.0.0.0/0"
    nat_gateway_id            = aws_nat_gateway.ngw_private_a[0].id
    depends_on                = [aws_route_table.private_route_table_a, aws_nat_gateway.ngw_private_a[0]]
}
resource "aws_route" "r50" {
    count                     = var.ngw
    route_table_id            = aws_route_table.private_route_table_b.id
    destination_cidr_block    = "0.0.0.0/0"
    nat_gateway_id            = aws_nat_gateway.ngw_private_b[0].id
    depends_on                = [aws_route_table.private_route_table_b, aws_nat_gateway.ngw_private_b[0]]
}
resource "aws_route" "r60" {
    count                     = var.ngw * var.three_az
    route_table_id            = aws_route_table.private_route_table_c[0].id
    destination_cidr_block    = "0.0.0.0/0"
    nat_gateway_id            = aws_nat_gateway.ngw_private_c[0].id
    depends_on                = [aws_route_table.private_route_table_c, aws_nat_gateway.ngw_private_c[0]]
}

# data_routes