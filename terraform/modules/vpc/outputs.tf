# vpc
output "vpc_id" {
    value = aws_vpc.main.id
}
output "vpc_cidr" {
    value = aws_vpc.main.cidr_block
}

# subnets
output "subnet_public_a_id" {
    value = aws_subnet.subnet_public_a.id
}
output "subnet_public_b_id" {
    value = aws_subnet.subnet_public_b.id
}
output "subnet_public_c_id" {
    value = var.three_az == 1 ? aws_subnet.subnet_public_c[0].id : "0"
}
output "subnet_private_a_id" {
    value = aws_subnet.subnet_private_a.id
}
output "subnet_private_b_id" {
    value = aws_subnet.subnet_private_b.id
}
output "subnet_private_c_id" {
    value = var.three_az == 1 ? aws_subnet.subnet_private_c[0].id : "0"
}
output "subnet_data_a_id" {
    value = var.include_data_subnets == 1 ? aws_subnet.subnet_data_a[0].id : "0"
}
output "subnet_data_b_id" {
    value = var.include_data_subnets == 1 ? aws_subnet.subnet_data_b[0].id : "0"
}
output "subnet_data_c_id" {
    value = (var.three_az * var.include_data_subnets) == 1 ? aws_subnet.subnet_data_c[0].id : "0"
}

# route tables
output "route_table_private_a_id" {
    value = aws_route_table.private_route_table_a.id
}

output "route_table_private_b_id" {
    value = aws_route_table.private_route_table_b.id
}

output "route_table_private_c_id" {
    value = aws_route_table.private_route_table_c[0].id
}

# gateway
output "ngw_private_a_eip" {
    value = aws_eip.ngw_eip_a
}
output "ngw_private_b_eip" {
    value = aws_eip.ngw_eip_b
}
output "ngw_private_c_eip" {
    value = var.three_az == 1 ? aws_eip.ngw_eip_c : []
}
