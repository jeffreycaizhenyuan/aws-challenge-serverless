# resources
resource "aws_subnet" "subnet_public_a" {
    vpc_id     = aws_vpc.main.id
    availability_zone = "${data.aws_region.current.name}a"

    cidr_block = var.subnet_public_a_cidr
    tags = {
        Name = "public_subnet_a"
    }
}
resource "aws_subnet" "subnet_public_b" {
    vpc_id     = aws_vpc.main.id
    availability_zone = "${data.aws_region.current.name}b"

    cidr_block = var.subnet_public_b_cidr
    tags = {
        Name = "public_subnet_b"
    }
}
resource "aws_subnet" "subnet_public_c" {
    count = var.three_az
    vpc_id     = aws_vpc.main.id
    availability_zone = "${data.aws_region.current.name}c"

    cidr_block = var.subnet_public_c_cidr
    tags = {
        Name = "public_subnet_c"
    }
}
resource "aws_subnet" "subnet_private_a" {
    vpc_id     = aws_vpc.main.id
    availability_zone = "${data.aws_region.current.name}a"

    cidr_block = var.subnet_private_a_cidr
    tags = {
        Name = "private_subnet_a"
    }
}
resource "aws_subnet" "subnet_private_b" {
    vpc_id     = aws_vpc.main.id
    availability_zone = "${data.aws_region.current.name}b"

    cidr_block = var.subnet_private_b_cidr
    tags = {
        Name = "private_subnet_b"
    }
}
resource "aws_subnet" "subnet_private_c" {
    count = var.three_az
    vpc_id     = aws_vpc.main.id
    availability_zone = "${data.aws_region.current.name}c"

    cidr_block = var.subnet_private_c_cidr
    tags = {
        Name = "private_subnet_c"
    }
}
resource "aws_subnet" "subnet_data_a" {
    count      = var.include_data_subnets
    vpc_id     = aws_vpc.main.id
    availability_zone = "${data.aws_region.current.name}a"

    cidr_block = var.subnet_data_a_cidr
    tags = {
        Name = "data_subnet_a"
    }
}
resource "aws_subnet" "subnet_data_b" {
    count      = var.include_data_subnets
    vpc_id     = aws_vpc.main.id
    availability_zone = "${data.aws_region.current.name}b"

    cidr_block = var.subnet_data_b_cidr
    tags = {
        Name = "data_subnet_b"
    }
}
resource "aws_subnet" "subnet_data_c" {
    count      = var.include_data_subnets * var.three_az
    vpc_id     = aws_vpc.main.id
    availability_zone = "${data.aws_region.current.name}c"

    cidr_block = var.subnet_data_c_cidr
    tags = {
        Name = "data_subnet_c"
    }
}
