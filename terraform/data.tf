data "aws_route53_zone" "serverless_hosted_zone" {
  name         = "${var.domain_name}."
  private_zone = false
}