resource "aws_alb" "serverless_alb" {
  name               = "serverless-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb_sg.id]
  subnets            = [module.vpc.subnet_public_a_id, module.vpc.subnet_public_b_id, module.vpc.subnet_data_c_id]
}

resource "aws_security_group" "alb_sg" {
  name        = "alb-sg"
  description = "ALB security group."
  vpc_id      = module.vpc.vpc_id

  # Ingress rule for HTTP - port 80
  ingress {
    description = "Allow HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Ingress rule for HTTPS - port 443
  ingress {
    description = "Allow HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Default egress rule: Allow all outbound traffic
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_lb_target_group" "lambda_tg" {
  name     = "lambda-target"
  target_type = "lambda"
}

resource "aws_lb_listener" "http_listener" {
  load_balancer_arn = aws_alb.serverless_alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.lambda_tg.arn
  }
}