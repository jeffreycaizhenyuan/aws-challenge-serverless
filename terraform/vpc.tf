module "vpc" {
  source = "./modules/vpc"

  vpc_cidr = local.vpc.vpc_cidr
  vpc_name = local.vpc.vpc_name

  igw = local.vpc.igw
  ngw = local.vpc.ngw
  three_az = local.vpc.three_az
  include_data_subnets = local.vpc.include_data_subnets

  subnet_public_a_cidr = local.vpc.subnet_public_a_cidr
  subnet_public_b_cidr = local.vpc.subnet_public_b_cidr
  subnet_public_c_cidr = local.vpc.subnet_public_c_cidr
  subnet_private_a_cidr = local.vpc.subnet_private_a_cidr
  subnet_private_b_cidr = local.vpc.subnet_private_b_cidr
  subnet_private_c_cidr = local.vpc.subnet_private_c_cidr
  subnet_data_a_cidr = local.vpc.subnet_data_a_cidr
  subnet_data_b_cidr = local.vpc.subnet_data_b_cidr
  subnet_data_c_cidr = local.vpc.subnet_data_c_cidr
}