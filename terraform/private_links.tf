resource "aws_vpc_endpoint" "s3" {
  vpc_id       = module.vpc.vpc_id
  service_name = "com.amazonaws.ap-southeast-2.s3"

  route_table_ids = [module.vpc.route_table_private_a_id, module.vpc.route_table_private_b_id, module.vpc.route_table_private_c_id]

  policy = <<POLICY
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Action": "*",
      "Effect": "Allow",
      "Resource": "*",
      "Principal": "*"
    }
  ]
}
POLICY

  tags = {
    Name = "my-s3-vpc-endpoint"
  }
}
