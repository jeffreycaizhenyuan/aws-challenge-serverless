resource "aws_iam_policy" "lambda_cloudwatch_logs" {
  name        = "lambda_cloudwatch_logs"
  description = "IAM policy for CloudWatch Logs access from Lambda"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = [
          "logs:CreateLogGroup",
          "logs:CreateLogStream",
          "logs:PutLogEvents"
        ],
        Resource = "arn:aws:logs:*:*:*",
      },
    ],
  })
}

resource "aws_iam_role_policy_attachment" "lambda_cloudwatch_logs" {
  role       = aws_iam_role.lambda.name
  policy_arn = aws_iam_policy.lambda_cloudwatch_logs.arn
}

#resource "aws_cloudwatch_event_rule" "lambda_warm_up_rule" {
#  name                = "lambda-warm-up-rule"
#  description         = "Triggers Lambda every 5 minutes to keep it warm"
#  schedule_expression = "rate(5 minutes)"
#}
#
#resource "aws_cloudwatch_event_target" "lambda_warm_up_target" {
#  rule      = aws_cloudwatch_event_rule.lambda_warm_up_rule.name
#  target_id = "LambdaTarget"
#  arn       = aws_lambda_function.dynamic_site.arn
#}

