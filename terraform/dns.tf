resource "aws_route53_record" "alias_route53_record" {
  zone_id = data.aws_route53_zone.serverless_hosted_zone.id
  name    = var.domain_name
  type    = "A"

  alias {
    name                   = aws_alb.serverless_alb.dns_name
    zone_id                = aws_alb.serverless_alb.zone_id
    evaluate_target_health = true
  }
}