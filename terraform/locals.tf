locals {
  vpc = {
    vpc_name = "aws_challenge_2024"
    vpc_cidr = "172.16.0.0/20"
    region = "ap-southeast-2"
    three_az = 1
    igw = 1
    ngw = 0
    include_data_subnets = 1

    subnet_public_a_cidr = "172.16.0.0/24"
    subnet_public_b_cidr = "172.16.1.0/24"
    subnet_public_c_cidr = "172.16.2.0/24"
    subnet_private_a_cidr = "172.16.3.0/24"
    subnet_private_b_cidr = "172.16.4.0/24"
    subnet_private_c_cidr = "172.16.5.0/24"
    subnet_data_a_cidr = "172.16.6.0/25"
    subnet_data_b_cidr = "172.16.6.128/25"
    subnet_data_c_cidr = "172.16.7.0/25"
  }
}